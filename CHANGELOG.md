## Version 2.0.1

* Fix ordering issue (thanks to Thodoris Sotiropoulos)

## Version 2.0.0

* Important rewrite to run on Puppet 4

## Version 1.3.1

* Added a LICENSE
* fixed typos in the README

## Version 1.3

* You can now override the default grub parameters with `grub_override`

## Version 1.2

* Fixed typos that prevented the module to run

## Version 1.1

* Uses file_line instead of replacing the whole /etc/default/grub file
* depends on puppetlabs-stdlib

## Version 1.0

* initial commit
