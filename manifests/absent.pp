class plymouth::absent {

  package { ['plymouth', 'plymouth-themes']:
    ensure => purged;
  }

  file_line { 'disable grub splash':
    ensure => present,
    path   => '/etc/default/grub',
    line   => 'GRUB_CMDLINE_LINUX_DEFAULT="quiet"',
    match  => '^GRUB_CMDLINE_LINUX_DEFAULT',
    notify => Exec['update-grub2'];
  }
}
